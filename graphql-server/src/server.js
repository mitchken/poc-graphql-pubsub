const express = require('express');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const bodyParser = require('body-parser');
const { makeExecutableSchema } = require('graphql-tools');
const { graphqls2s } = require('graphql-s2s');
const glue = require('schemaglue');
const app = express();
const PORT = 3000;

const { schema, resolver } = glue('src/graphql');

const executableSchema = makeExecutableSchema({
  typeDefs: graphqls2s.transpileSchema(schema),
  resolvers: resolver,
});

// The GraphQL endpoint
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: executableSchema }));

// GraphiQL, a visual editor for queries
app.use(
  '/graphiql',
  graphiqlExpress({
    endpointURL: '/graphql',
  }),
);

app.listen(PORT, () => {
  console.log(`GraphQL Server is now running on http://localhost:3000`);
  // Set up the WebSocket for handling GraphQL subscriptions
});

process.on('uncaughtException', function (err) {
  console.log(err);
});