const { PubSub } = require('graphql-subscriptions');
const _ = require('lodash');
const sender = require('../../send');
sender.start();
const itemStore = [
  {
    identifier: '1000aaaa',
    title: 'eerste titel',
    content: 'langere tekst',
  },
  {
    identifier: '2000bbbb',
    title: 'tweede titel',
    content: 'langere tekst',
  },
  {
    identifier: '3000ccc',
    title: 'derde titel',
    content: 'langere tekst',
  },
  {
    identifier: '4000ddd',
    title: 'vierde titel',
    content: 'langere tekst',
  },
];

const resolveItem = async identifier => {
  let response = {};
  if (identifier) {
    response = _.find(itemStore, ['identifier', identifier]);
  }
  return response;
};

const createItem = (parent, input) => {
  const newItem = {
    identifier: _.random(0, 99999999) + 'identifier',
    title: input.title,
    content: input.content,
  };
  sender.send('Item', newItem);
  itemStore.push(newItem);
  return newItem;
};

const updateItem = (item, input) => {
  const updatedProduct = Object.assign(item, input);
  const index = _.findIndex(itemStore, ['identifier', input.identifier]);
  const newItem = {
    identifier: updatedProduct.identifier,
    title: updatedProduct.title,
    content: updatedProduct.content,
  };
  _.set(itemStore, index, newItem);

  // publishes the update to the itemUpdated channel
  // pubsub.publish('itemUpdated', { itemUpdated: newItem, channelId: 'channel1' });

  return newItem;
};

const deleteItem = () => true;

const pubsub = new PubSub();

// export module functions
module.exports = {
  // list all graphql resolvers
  resolver: {
    Query: {
      item: (parent, { identifier }) => resolveItem(identifier),
    },
    Mutation: {
      item: (parent, { identifier }) => resolveItem(identifier),
    },
    ItemOps: {
      createItem: (parent, { input }) => createItem(parent, input),
      updateItem: (parent, { input }) => updateItem(parent, input),
      deleteItem: (parent, { input }) => deleteItem(parent, input),
    },
  },
};
