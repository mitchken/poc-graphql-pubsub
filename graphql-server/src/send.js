const amqp = require('amqplib/callback_api');
let amqpConn = null;
let pubChannel = null;
let offlinePubQueue = [];

const start = () => {
  amqp.connect(
    'amqp://guest:guest@172.20.0.1:5672/',
    function(err, conn) {
      if (err) {
        console.error('[AMQP]', err.message);
        return setTimeout(start, 1000);
      }
      conn.on('error', function(err) {
        if (err.message !== 'Connection closing') {
          console.error('[AMQP] conn error', err.message);
        }
      });
      conn.on('close', function() {
        console.error('[AMQP] reconnecting');
        return setTimeout(start, 1000);
      });
      console.log('[AMQP] connected');
      amqpConn = conn;
      whenConnected();
    },
  );
};

const closeOnErr = err => {
  if (!err) return false;
  console.error('[AMQP] error', err);
  amqpConn.close();
  return true;
};

const whenConnected = () => {
  amqpConn.createConfirmChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on('error', function(err) {
      console.error('[AMQP] channel error', err.message);
    });
    ch.on('close', function() {
      console.log('[AMQP] channel closed');
    });

    pubChannel = ch;
    while (true) {
      var m = offlinePubQueue.shift();
      if (!m) break;
      publish(m[0], m[1], m[2]);
    }
  });
};

const publish = (exchange, routingKey, content) => {
  try {
    pubChannel.publish(exchange, routingKey, content, { persistent: true }, (err, ok) => {
      if (err) {
        console.error('[AMQP] publish', err);
        offlinePubQueue.push([exchange, routingKey, content]);
        pubChannel.connection.close();
      } else {
        console.log('published to channel', routingKey);
      }
    });
  } catch (e) {
    console.error('[AMQP] publish', e.message);
    offlinePubQueue.push([exchange, routingKey, content]);
  }
};

const send = (type, content) => {
  // actual publish to rabbitMQ
  publish('', 'allChanges', new Buffer(JSON.stringify({ type, content })));
};

module.exports = { send, start };
