# POC graphql-publications

## Introduction
The goal of this project is to test the usability of graphql Pub/Sub.

We would like to have an easy to plug in publication model which is easy to reuse/expand.

## Content Sources

All different classes which are defined, in this example only 1 is defined.


## Reproduce

Run

```
npm install
node index.js
```

The project should now be running on port 3000.

Browse to localhost:3000/graphiql

Query to start a subscription

```
subscription {
  itemUpdated(channelId: "channel1") {
    identifier
  }
}
```

Example mutation:
```
mutation {
  item {
    updateItem(input: {identifier: "1000aaaa", title: "nieuwe titel", content:"test32"}) {
      identifier
      content,
      title
    }
  }
}

```


## Code Organisation

The application is being mounted in schema.js, which does the heavy lifting.
The item resolver has been added for all Item operations.





