const amqp = require('amqplib/callback_api');
let amqpConn = null;
let pubChannel = null;

const start = () => {
  amqp.connect(
    'amqp://guest:guest@172.20.0.1:5672/',
    function(err, conn) {
      if (err) {
        console.error('[AMQP]', err.message);
        return setTimeout(start, 1000);
      }
      conn.on('error', function(err) {
        if (err.message !== 'Connection closing') {
          console.error('[AMQP] conn error', err.message);
        }
      });
      conn.on('close', function() {
        console.error('[AMQP] reconnecting');
        return setTimeout(start, 1000);
      });
      console.log('[AMQP] connected');
      amqpConn = conn;
      whenConnected();
    },
  );
};

const closeOnErr = err => {
  if (!err) return false;
  console.error('[AMQP] error', err);
  amqpConn.close();
  return true;
};

//start listening
const whenConnected = () => {
  amqpConn.createChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on('error', function(err) {
      console.error('[AMQP] channel error', err.message);
    });
    ch.on('close', function() {
      console.log('[AMQP] channel closed');
    });
    pubChannel = ch;
    ch.prefetch(10);
    ch.assertQueue('allChanges', { durable: true }, function(err, _ok) {
      if (closeOnErr(err)) return;
      ch.consume('allChanges', processMsg, { noAck: false });
      console.log('Worker is started');
    });
  });
};

const processMsg = msg => {
  doActualWork(msg, function(ok) {
    try {
      if (ok) pubChannel.ack(msg);
      else pubChannel.reject(msg, true);
    } catch (e) {
      closeOnErr(e);
    }
  });
};

const doActualWork = (msg, cb) => {
  console.log('Received ', msg.content.toString());
  cb(true);
};

start();
