# POC graphql-rabbitMQ

## Introduction
The goal of this project is to test the usability of graphql with rabbitMQ.

We would like to have an easy to plug in publication model which is easy to reuse/expand when graphQL is expanded further.

## Reproduce

Run

```
docker-compose up
```

The graphql project should now be running on port 3000.

Browse to localhost:3000/graphiql

Query to start a subscription

Example mutation:
```
mutation {
  item {
    createItem(input: {title: "nieuwe titel", content:"test32"}) {
      identifier
      content,
      title
    }
  }
}
```

## RabbitMQ

The management console is available on localhost:15672
Username: guest
Password: guest

## Conclusion

RabbitMQ can provide a scalable and trusthworthy message queue for this purpose.
Messages need acknowledgement of being processed which guarantees consistency. 






